const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const isEnv = () => process.env.NODE_ENV === 'dev'
const _resolve = (prePath, nextPath) => path.resolve(prePath, nextPath)


module.exports = {
    mode: 'development',
    context: _resolve(__dirname, '../src'),
    entry: {
        main: _resolve(__dirname, '../src/entry/main.js')
    },
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, '../dist'),
        clean: !isEnv(),
        hashDigestLength: 5,
        assetModuleFilename: '[name][contenthash][ext]',
        filename: 'js/[name].[contenthash].js'
    },
    resolve: {
        extensions: ['.vue', '.js', '.json'],
        alias: {
            '@': _resolve(__dirname, '../src')
        }
    },
    target: 'web',
    externals: {
        'vue': 'Vue',
        'vue-router': 'VueRouter',
        'vuex': 'Vuex'
    },
    devtool: isEnv() ? 'cheap-module-source-map' : false,
    devServer: {
        static: _resolve(__dirname, '../dist'),
        hot: true,
        port: 9527,
        client: {
            overlay: false,
        },
        compress: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: _resolve(__dirname, '../public/index.html'),
            title: 'webpack5'
        }),
        new MiniCssExtractPlugin({
            filename: 'style/[name].[contenthash].css',
            chunkFilename: 'style/[name].[contenthash].css'
        }),
        new VueLoaderPlugin()
    ],
    module: {
        rules: [
            {
                test: /.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": [
                            "@babel/preset-env"
                        ],
                        "plugins": [
                            [
                                "@babel/plugin-transform-runtime"
                            ]
                        ]
                    }
                }
            },
            {
                test: /.vue$/,
                use: 'vue-loader'
            },
            {
                test: /.jpg|.png|.svg$/,
                type: 'asset',
                parser: {
                    dataUrlCondition: {
                        // 10kb以下的资源进行base64转码
                        maxSize: 10 * 1024
                    }
                },
                generator: {
                    filename: 'images/[name][contenthash][ext]'
                }
            },
            {
                test: /.scss$/,
                use: [
                    isEnv() ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        require('autoprefixer')
                                    ]
                                ]
                            }

                        }
                    },
                    'sass-loader'
                ]
            },
            {
                test: /.html$/,
                use: 'html-loader'
            }
        ]
    },
    optimization: {
        minimizer: [
            '...',
            new CssMinimizerWebpackPlugin()
        ],
        splitChunks: {
            cacheGroups: {
                vender: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    }
}