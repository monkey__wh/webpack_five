import VueRouter from 'vue-router';

const routes = [
    {
        path: '/',
        component: () => import('@/views/main')
    }
]

export default new VueRouter({
    routes
})
